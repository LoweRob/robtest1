@echo off

echo Requires Visual Stuido Build Tools 2015

echo Rebuilding Solution
"%ProgramFiles(x86)%\MSBuild\14.0\Bin\MSBuild.exe" "C:\Source\RobTest1\Database2\Database2.sln" /t:Clean,Build 

echo Publishing Project
"%ProgramFiles(x86)%\MSBuild\14.0\Bin\MSBuild.exe" /t:Publish /p:SqlPublishProfilePath="C:\Source\RobTest1\Database2\Database2\localrobtest.publish.xml" "C:\Source\RobTest1\Database2\Database2\Database2.sqlproj"

pause